import 'package:astronomy_fun/Screens/homepage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: bgColor,
        backgroundColor: bgColor,
        accentColor: Color(0xff8E97FD),
        textTheme: TextTheme(
            button: TextStyle(
              fontSize: 14,
              color: Color(0xffE6E7F2),
            ),
            bodyText1: TextStyle(fontFamily: "Lato")),
      ),
      home: HomePage(),
    );
  }
}

Map<int, Color> backgroundColor = {
  50: Color(0xff03174C).withOpacity(0.1),
  100: Color(0xff03174C).withOpacity(.2),
  200: Color(0xff03174C).withOpacity(.3),
  300: Color(0xff03174C).withOpacity(.4),
  400: Color(0xff03174C).withOpacity(.5),
  500: Color(0xff03174C).withOpacity(.6),
  600: Color(0xff03174C).withOpacity(.7),
  700: Color(0xff03174C).withOpacity(.8),
  800: Color(0xff03174C).withOpacity(.9),
  900: Color(0xff03174C).withOpacity(1),
};
MaterialColor bgColor = MaterialColor(0xff03174C, backgroundColor);

Color textColor = Color(0xffE6E7F2);
