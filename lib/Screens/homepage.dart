import 'package:astronomy_fun/Screens/HomeMain.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double top = 200;
  double opacity = 0;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 0), () {
      setState(() {
        top = 40;
        opacity = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              Flexible(
                flex: 10,
                child: Center(
                  child: AnimatedOpacity(
                    duration: Duration(milliseconds: 1300),
                    curve: Curves.linear,
                    opacity: opacity,
                    child: Hero(
                      tag: "hometolifeline",
                      transitionOnUserGestures: true,
                      child: Material(
                        type: MaterialType.transparency,
                        child: Text(
                          "Astronomy".toUpperCase(),
                          style: TextStyle(
                            fontFamily: "GoodUnicorn",
                            color: Colors.white,
                            fontSize: 50,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 4,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    AnimatedPositioned(
                      duration: Duration(milliseconds: 1800),
                      top: top,
                      curve: Curves.elasticInOut,
                      child: AnimatedOpacity(
                        duration: Duration(milliseconds: 1400),
                        opacity: opacity,
                        child: TextButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 120, vertical: 20)),
                            backgroundColor: MaterialStateProperty.all(Theme.of(context).accentColor),
                            overlayColor: MaterialStateProperty.all(Colors.white30),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(38.0),
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageRouteBuilder(
                                    opaque: false, transitionDuration: Duration(milliseconds: 1200), pageBuilder: (_, __, ___) => HomeMain()));
                          },
                          child: Text(
                            "GET STARTED",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
