import 'dart:convert';

import 'package:astronomy_fun/Screens/All.dart';
import 'package:astronomy_fun/Screens/CommonWidgets.dart/ButtonTiles.dart';
import 'package:astronomy_fun/Screens/Planets.dart';
import 'package:astronomy_fun/Screens/ProtoPlanet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeMain extends StatefulWidget {
  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {
  List girdData = [];
  Future<void> loadJsonData() async {
    var jsonText = await (rootBundle.loadString('assets/json/data.json'));
    setState(() {
      girdData = json.decode(jsonText);
    });
  }

  @override
  void initState() {
    super.initState();
    loadJsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/home1Bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: Hero(
                  tag: "hometolifeline",
                  transitionOnUserGestures: true,
                  child: Material(
                    type: MaterialType.transparency,
                    child: Text(
                      "Astronomy".toUpperCase(),
                      style: TextStyle(
                        fontFamily: "GoodUnicorn",
                        color: Colors.white,
                        fontSize: 50,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32.0),
                child: TextField(
                  cursorColor: Colors.white,
                  strutStyle: StrutStyle(),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(left: 24.0, right: 10),
                      child: Icon(
                        Icons.search,
                        color: Color(0xff586894),
                      ),
                    ),
                    hintText: "Search a planet here.......",
                    hintStyle: TextStyle(
                      color: Color(0xff586894),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25),
                      borderSide: BorderSide(
                        color: Color(0xff586894),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25),
                      borderSide: BorderSide(
                        color: Color(0xff586894),
                      ),
                    ),
                  ),
                  style: TextStyle(
                    color: Color(0xffE6E7F2),
                    fontSize: 16,
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: [
                    SpaceFun(
                      background: "assets/images/spacefun.png",
                      title: "The Space Fun",
                      subtitle: "View all the planet with detail\ndescription",
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => All()));
                      },
                    ),
                    SizedBox(height: 32),
                    SpaceFun(
                      background: "assets/images/home/solarsystemBg.png",
                      title: "The Solar system",
                      subtitle: "Birth of\nSolar system",
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Protoplanet()));
                      },
                    ),
                    SizedBox(height: 32),
                    GridView.count(
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 25,
                      physics: BouncingScrollPhysics(),
                      childAspectRatio: 1.1,
                      children: [
                        ...girdData.map((planet) {
                          return Builder(builder: (BuildContext context) {
                            return GridPlanetButton(
                              planet,
                              planet['name'],
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Planets(data: planet, heroTag: planet['name'])));
                              },
                            );
                          });
                        }),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
