import 'package:flutter/material.dart';
import 'package:astronomy_fun/utils/Capitaization.dart';

class SpaceFun extends StatelessWidget {
  Function onPressed;
  String background, title, subtitle;
  SpaceFun({this.onPressed, this.background, this.title, this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Flexible(
            flex: 2,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    this.title,
                    style: TextStyle(
                      fontFamily: "Lato",
                      fontWeight: FontWeight.bold,
                      fontSize: 36,
                      color: Color(0xffFFE7BF),
                    ),
                  ),
                  Text(
                    this.subtitle,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: "Lato",
                      fontWeight: FontWeight.w100,
                      fontSize: 14,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 21),
          Flexible(
            flex: 1,
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 15, vertical: 10)),
                backgroundColor: MaterialStateProperty.all(Colors.white),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(38.0),
                  ),
                ),
              ),
              onPressed: this.onPressed,
              child: Text(
                "START",
                textAlign: TextAlign.center,
                style: TextStyle(color: Color(0xff3F414E), fontSize: 14, fontWeight: FontWeight.normal),
              ),
            ),
          ),
        ],
      ),
      height: 233,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(this.background),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class GridPlanetButton extends StatelessWidget {
  String fg, bg, title, subtitle, tag;
  Function onPressed;
  GridPlanetButton(var data, var tag, {this.onPressed}) {
    this.fg = data['fg'];
    this.bg = data['bg'];
    this.title = data['name'];
    this.subtitle = data['distance'];
    this.tag = tag;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onPressed,
      splashColor: Colors.white,
      focusColor: Colors.white,
      enableFeedback: true,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 14),
              child: Center(
                child: Hero(
                  tag: this.tag,
                  child: Image(
                    image: AssetImage(this.fg),
                    height: 75,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: AssetImage(this.bg),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 10),
            Text(
              this.title,
              style: TextStyle(color: Color(0xffE6E7F2), fontSize: 18),
            ),
            Text(
              this.subtitle.capitalize(),
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Color(0xff98A1BD), fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }
}
