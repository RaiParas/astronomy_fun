import 'package:flutter/material.dart';
import 'package:translator/translator.dart';

class Planets extends StatefulWidget {
  var data;
  String heroTag;
  Planets({this.data, this.heroTag});
  @override
  _PlanetsState createState() => _PlanetsState();
}

class _PlanetsState extends State<Planets> {
  String name, distance, information;

  bool isNepali = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      name = widget.data['name'];
      distance = widget.data['distance'];
      information = widget.data['description'];
    });
  }

  Future<void> translate() async {
    var translator = GoogleTranslator();
    translator.translate(name, from: this.isNepali ? 'ne' : 'en', to: this.isNepali ? 'en' : 'ne').then((s) => {
          setState(() {
            name = s.toString();
          })
        });
    translator.translate(distance, from: this.isNepali ? 'ne' : 'en', to: this.isNepali ? 'en' : 'ne').then((s) => {
          setState(() {
            distance = s.toString();
          })
        });
    translator.translate(information, from: this.isNepali ? 'ne' : 'en', to: this.isNepali ? 'en' : 'ne').then((s) => {
          setState(() {
            information = s.toString();
          })
        });
    setState(() {
      this.isNepali = !this.isNepali;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF03174C),
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
              ),
              image: DecorationImage(
                image: AssetImage(widget.data['bg']),
                fit: BoxFit.fill,
              ),
            ),
            child: Stack(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 20, 0),
                    child: Hero(
                      tag: widget.heroTag,
                      child: Image(
                        image: AssetImage(widget.data['fg']),
                        height: 200,
                        width: 200,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 25,
                  top: 40,
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: FloatingActionButton(
                      heroTag: null,
                      foregroundColor: Colors.black54,
                      backgroundColor: Colors.white,
                      elevation: 0,
                      tooltip: 'back',
                      child: Icon(Icons.arrow_back, size: 35),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: FloatingActionButton(
                            heroTag: null,
                            foregroundColor: Colors.white,
                            backgroundColor: Color(0X33000000),
                            elevation: 0,
                            tooltip: 'Love',
                            child: Image(
                              image: AssetImage("assets/images/icons/favIcon.png"),
                              height: 18.26,
                              width: 16.14,
                            ),
                            onPressed: () {
                              print('Clicked');
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: SizedBox(
                            height: 60,
                            width: 60,
                            child: FloatingActionButton(
                              heroTag: null,
                              foregroundColor: Colors.white,
                              backgroundColor: Color(0X33000000),
                              tooltip: "Download",
                              elevation: 0,
                              child: Image(
                                image: AssetImage("assets/images/icons/download.png"),
                                height: 18.5,
                                width: 18,
                              ),
                              onPressed: () {
                                print('Clicked');
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: FloatingActionButton(
                            heroTag: null,
                            foregroundColor: Colors.white,
                            backgroundColor: Color(0X33000000),
                            elevation: 0,
                            tooltip: 'Translate',
                            child: Image(
                              image: AssetImage("assets/images/icons/translate.png"),
                              height: 24,
                              width: 24,
                            ),
                            onPressed: this.translate,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      this.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 34,
                        fontFamily: "Lato",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        this.distance,
                        style: TextStyle(
                          color: Color(0xff98A1BD),
                          fontSize: 12,
                          fontFamily: "Lato",
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          this.information +
                              '''
                            khiofhsdohfashodf
                            lk dfhksa dkfkhjosad fdsoijfosd kfjosdhjfoas lkfhods fosdflkhsaiodhfoskd foh doifhoasdhfksab
                            jkfbiasdghf ksjdfisdfkjs dfisdhfkbjdsuifghsivbmljxc bvdgfmbnsv 
                            djkhisaghdf;kj sdhfuihdifhjksdh fksdb ffkmsdbfuisdbfmn k fkjsda dufisdbmnsbcviusbdfadsifhwefgh
                            kjfhbjdsghf;skdjhfaiuhdfkjsdhfksdjfhkhsadjkhfk asjdhf sd
                            fsdfhjkhsdifhskdhfs'
                            kdhf;sadhfsakjh vcxlmv vkcxmvlmsvlsmdlam 
                            ''',
                          style: TextStyle(
                            color: Color(0xff98A1BD),
                            fontSize: 16,
                            fontFamily: "Lato",
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
