import 'dart:ffi';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:astronomy_fun/utils/Capitaization.dart';

class All extends StatefulWidget {
  @override
  _AllState createState() => _AllState();
}

class _AllState extends State<All> {
  CarouselController carouselController = CarouselController();
  var planets = [
    {
      "image": "assets/images/planets/mercury.png",
      "name": "Mercury",
      "diameter": "4,879 kilometers in Diameter",
      "information":
          '''Mercury is the smallest planet in the Solar System and the closest to the Sun. Its orbit around the Sun takes 87.97 Earth days, the shortest of all the Sun's planets. It is named after the Roman god Mercurius (Mercury), god of commerce, messenger of the gods, and mediator between gods and mortals, corresponding to the Greek god Hermes (Ἑρμῆς). Like Venus, Mercury orbits the Sun within Earth's orbit as an inferior planet, and its apparent distance from the Sun as viewed from Earth never exceeds 28°. This proximity to the Sun means the planet can only be seen near the western horizon after sunset or the eastern horizon before sunrise, usually in twilight. At this time, it may appear as a bright star-like object but is often far more difficult to observe than Venus. From Earth, the planet telescopically displays the complete range of phases, similar to Venus and the Moon, which recurs over its synodic period of approximately 116 days.''',
    },
    {
      "image": "assets/images/planets/venus.png",
      "name": "Venus",
      "diameter": "12,104 kilometers in Diameter",
      "information":
          '''Venus is the second planet from the Sun. It is named after the Roman goddess of love and beauty. As the brightest natural object in Earth's night sky after the Moon, Venus can cast shadows and can be, on rare occasions, visible to the naked eye in broad daylight. Venus lies within Earth's orbit, and so never appears to venture far from the Sun, either setting in the west just after dusk or rising in the east a little while before dawn. Venus orbits the Sun every 224.7 Earth days. With a rotation period of 243 Earth days, it takes significantly longer to rotate about its axis than any other planet in the Solar System, and does so in the opposite direction to all but Uranus (meaning the Sun rises in the west and sets in the east). Venus does not have any moons, a distinction it shares only with Mercury among the planets in the Solar System.''',
    },
    {
      "image": "assets/images/planets/earth.png",
      "name": "Earth",
      "diameter": "12,756 kilometers in Diameter",
      "information":
          '''Earth, our home, is the third planet from the sun. It's the only planet known to have an atmosphere containing free oxygen, oceans of water on its surface and, of course, life.Earth is the fifth largest of the planets in the solar system. It's smaller than the four gas giants — Jupiter, Saturn, Uranus and Neptune — but larger than the three other rocky planets, Mercury, Mars and Venus.Earth has a diameter of roughly 8,000 miles (13,000 kilometers) and is round because gravity pulls matter into a ball. But, it's not perfectly round. Earth is really an "oblate spheroid," because its spin causes it to be squashed at its poles and swollen at the equator.''',
    },
    {
      "image": "assets/images/planets/mars.png",
      "name": "Mars",
      "diameter": "3,475 kilometers in Diameter",
      "information":
          '''Mars is the fourth planet from the Sun and the second-smallest planet in the Solar System, being larger than only Mercury. In English, Mars carries the name of the Roman god of war and is often referred to as the "Red Planet". The latter refers to the effect of the iron oxide prevalent on Mars's surface, which gives it a reddish appearance distinctive among the astronomical bodies visible to the naked eye. Mars is a terrestrial planet with a thin atmosphere, with surface features reminiscent of the impact craters of the Moon and the valleys, deserts and polar ice caps of Earth.''',
    },
  ];
  int _currentPage = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        child: Column(
          children: [
            Flexible(
              flex: 2,
              child: Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.symmetric(vertical: 6),
                child: CarouselSlider.builder(
                  carouselController: carouselController,
                  options: CarouselOptions(
                    height: 200.0,
                    viewportFraction: 0.25,
                    aspectRatio: 1,
                    enableInfiniteScroll: false,
                    initialPage: 2,
                    enlargeStrategy: CenterPageEnlargeStrategy.scale,
                    enlargeCenterPage: true,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _currentPage = index;
                      });
                    },
                  ),
                  itemBuilder: (context, index, realIndex) => Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    alignment: Alignment.center,
                    child: InkWell(
                      onTap: () {
                        print("pressed");
                        carouselController.animateToPage(index);
                      },
                      child: Image(
                        image: AssetImage(planets[index]['image']),
                        width: double.infinity,
                      ),
                    ),
                  ),
                  itemCount: planets.length,
                ),
              ),
            ),
            Flexible(
              flex: 3,
              child: Container(
                color: Color(0xff0C225F),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FadeInText(
                      text: planets[_currentPage]['name'],
                      style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: "Lato"),
                    ),
                    SizedBox(height: 16),
                    FadeInText(
                      text: planets[_currentPage]['diameter'].capitalize(),
                      style: TextStyle(fontSize: 12, color: Color(0xff98A1BD), fontFamily: "Lato"),
                    ),
                    SizedBox(height: 8),
                    SingleChildScrollView(
                      child: FadeInText(
                        text: planets[_currentPage]['information'],
                        style: TextStyle(fontSize: 12, color: Color(0xff98A1BD), fontFamily: "Lato", height: 1.5),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/spacefun/all.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

class FadeInText extends StatelessWidget {
  String text;
  TextStyle style;
  TextAlign textAlign;
  FadeInText({key, this.text, this.style, this.textAlign = TextAlign.left});
  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: Duration(milliseconds: 200),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return FadeTransition(
          opacity: animation.drive(CurveTween(curve: Curves.easeInCubic)),
          child: child,
        );
      },
      child: Text(
        this.text,
        key: ValueKey<String>(this.text),
        style: this.style,
        textAlign: this.textAlign,
      ),
    );
  }
}
