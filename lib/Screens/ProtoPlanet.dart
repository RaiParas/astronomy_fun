import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:zoom_widget/zoom_widget.dart';

const width = 250.0;

class Protoplanet extends StatelessWidget {
  var solarSystemLife = [
    {
      "image": "assets/images/protoplanetary/nebula.png",
      "name": "Nebula",
      "time": "About 4.6 billion years ago",
      "description": '''Nebula is an interstellar cloud of dust, hydrogen, helium and other ionized gases. 
        It is mostly remaining of star after its death. It is the gases clogged in a place. It is spread on the millions of km in diameter.
        Gradually, due to interaction of gases and  dust with each other creates charges between the gases and dusts. This result to gases and dust stuck together.
        As the mass increases, gravity plays a role to attract more masses. With increase in mass, increases pressure  and temperature.
        This start nuclear reaction in a middle of nebula. This gradully starts to spinning forming disc of gasses. This hot ignition of reaction in center of disc is starting phase of Sun.
        It is called proto sun.   
        '''
    },
    {
      "image": "assets/images/protoplanetary/protosun.png",
      "name": "Proto Sun",
      "time": "After billions of years",
      "description":
          '''After proto sun is formed. It begins to feed in the gases and dust around it. When feeding more and more fuel, the sun began to burn more bright.
        The gases and dust around the proto sun began to interact with each other. They collide with each other forming small heavenly bodies, sized of asteroids. They to collide with each other forming Planetesimals.'''
    },
    {
      "image": "assets/images/protoplanetary/planetesimals.jpeg",
      "name": "Planetesimals",
      "time": "After few millions years ",
      "description":
          '''Planetesimals revolve around the proto sun along with left gases and dusts. These bodies collide with each other consuming other gases present in the path of their revolution.
      When all the gases and dusts around the Planetesimals is consumed, they becomes proto-planets.
      '''
    },
    {
      "image": "assets/images/protoplanetary/protoplanet.png",
      "name": "Proto Planets",
      "time": "After few millions years",
      "description":
          '''When proto-planets are formed, the nearby planets collide with each other forming a larger planet and its Satellites. This is also true for Earth and our moon.
        In our solar system, Mercury and Venus has high temperature as they are to close to sun, the gases of nebula evaporated resulting in rocky planet. This also apply to Earth and mars. 
        But, after the distance of mars, the temperature of planet become to settle down, this cause Jupiter and Saturn to be gas gaints. 
        After Saturn, the distance of Sun is so far that temperature becomes freezing, resulting ice gaints like Uranus and Neptune. 
        The remaining dust of nebula are converted to astroid belt and drawf planets including Pluto.
      '''
    },
    {
      "image": "assets/images/protoplanetary/planets.png",
      "name": "Planets",
      "time": "After some time",
      "description": "The proto planets begain to cool down forming the present planet as we observe today."
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Text(
              "Birth of Solar system",
              style: TextStyle(color: Colors.white, fontSize: 32, fontFamily: "Lato"),
              textAlign: TextAlign.center,
            ),
            Text(
              "Based on Proto-Planet Hypotheses",
              style: TextStyle(color: Colors.white, fontSize: 10, fontFamily: "Lato"),
              textAlign: TextAlign.center,
            ),
            Expanded(
              child: Zoom(
                width: width * solarSystemLife.length,
                height: 300,
                initZoom: 0.1,
                canvasColor: Theme.of(context).backgroundColor,
                colorScrollBars: Colors.white24,
                backgroundColor: Theme.of(context).backgroundColor,
                enableScroll: true,
                scrollWeight: 10.0,
                opacityScrollBars: 1,
                child: Center(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    alignment: Alignment.centerLeft,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        ...solarSystemLife.map((stage) {
                          var index = solarSystemLife.indexOf(stage);

                          var topChild = index % 2 == 0
                              ? Column(
                                  children: [
                                    Image(
                                      image: AssetImage(stage['image']),
                                      height: 120,
                                    ),
                                    Text(
                                      stage['name'],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                )
                              : Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      height: 100,
                                      child: SingleChildScrollView(
                                        child: Text(
                                          stage['description'],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      stage['time'],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                );

                          var bottomChild = index % 2 == 0
                              ? Column(
                                  children: [
                                    Text(
                                      stage['time'],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Container(
                                      height: 100,
                                      child: SingleChildScrollView(
                                        child: Text(
                                          stage['description'],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Column(
                                  children: [
                                    Text(
                                      stage['name'],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Image(
                                      image: AssetImage(stage['image']),
                                      height: 120,
                                    ),
                                  ],
                                );
                          return TimeTile(
                            bottomChild: bottomChild,
                            topChild: topChild,
                            isFirst: index == 0,
                            isLast: index == (solarSystemLife.length - 1),
                          );
                        }),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TimeTile extends StatelessWidget {
  Widget topChild, bottomChild;
  bool isFirst, isLast;
  TimeTile({this.topChild, this.bottomChild, this.isFirst = false, this.isLast = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: TimelineTile(
        indicatorStyle: IndicatorStyle(color: Colors.deepPurple),
        beforeLineStyle: LineStyle(color: Colors.deepPurple),
        afterLineStyle: LineStyle(color: Colors.deepPurple),
        isFirst: this.isFirst,
        isLast: this.isLast,
        axis: TimelineAxis.horizontal,
        alignment: TimelineAlign.center,
        endChild: Container(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: this.bottomChild,
        ),
        startChild: Container(
          child: this.topChild,
          padding: EdgeInsets.symmetric(horizontal: 5),
        ),
      ),
    );
  }
}
